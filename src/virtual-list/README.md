# Flat List 

The flat list component utilizes [useVirtualList](https://vueuse.org/core/useVirtualList/) to facilitate high performance scrolling. 

## Resources

- https://vueuse.org/core/useVirtualList/
- https://css-tricks.com/position-vertical-scrollbars-on-opposite-side-with-css/