import { createMemoryHistory, createRouter } from 'vue-router'

import FlatVirtualList from "@/virtual-list/VirtualList.vue";
import TreeView from "@/tree-view/TreeView.vue";
import Overview from "@/Overview.vue";
import ItemDragDrop from "@/item-drag-drop/ItemDragDrop.vue";
import SideMenuView from "@/sidemenu/SideMenuView.vue";
import ModalView from "@/modal/ModalView.vue";

const routes = [
    { path: '/', redirect: '/tree-view' },
    { path: '/overview', component: Overview },
    { path: '/virtual-list', component: FlatVirtualList },
    { path: '/tree-view', component: TreeView },
    { path: '/item-drag-and-drop', component: ItemDragDrop },
    { path: '/sidemenu', component: SideMenuView },
    { path: '/modal', component: ModalView },
]

const router = createRouter({
    history: createMemoryHistory(),
    routes,
})

export default router