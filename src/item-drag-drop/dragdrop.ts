import {Ref, ref} from "vue";

type DragdropItem = {
    id: string
}

const draggedItemId = ref<string | null>(null)
const dragOverItemId = ref<string | null>(null)


export function useDragDrop(items: Ref<DragdropItem[]>) {

    function handleDragStart(item: DragdropItem) {
        draggedItemId.value = item.id
    }

    function handleDragEnter(item: DragdropItem) {
        dragOverItemId.value = item.id
    }

    function handleDragLeave() {
        dragOverItemId.value = null
    }

    function handleDragOver(e: Event) {
        e.preventDefault();
    }

    function handleDrop(e: Event, item: DragdropItem) {
        e.stopPropagation(); // stops the browser from redirecting.
        items.value = insertBefore([...items.value], draggedItemId.value!, item.id)
    }

    function handleDragEnd() {
        draggedItemId.value = null
        dragOverItemId.value = null
    }

    function insertBefore(arr: DragdropItem[], moveItemId: string, targetItemId: string): DragdropItem[] {
        const moveItemIndex = arr.findIndex(item => item.id === moveItemId);
        const targetItemIndex = arr.findIndex(item => item.id === targetItemId);
        if (moveItemIndex < 0 || targetItemIndex < 0) return arr
        const [itemToMove] = arr.splice(moveItemIndex, 1);
        arr.splice(targetItemIndex, 0, itemToMove);
        return arr;
    }

    return {
        draggedItemId,
        dragOverItemId,
        handleDragStart,
        handleDragEnter,
        handleDragLeave,
        handleDragOver,
        handleDrop,
        handleDragEnd,

    }
}
