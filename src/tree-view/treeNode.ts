import {TreeNode} from "@/tree-view/types.ts";

export function useTreeNode() {

    const collectDescendants = (node: TreeNode, leavesOnly = false): Set<TreeNode> => {
        if (leavesOnly) return _collectLeaves(node)
        return _collectDescendants(node);
    }

    function _collectLeaves(currentNode: TreeNode, acc = new Set<TreeNode>()): Set<TreeNode> {
        currentNode.children.forEach(child => {
            if (child.children.length === 0) {
                acc.add(child);
            } else {
                _collectLeaves(child, acc);
            }
        });

        return acc
    }

    function _collectDescendants(currentNode: TreeNode, acc = new Set<TreeNode>()): Set<TreeNode> {
        currentNode.children.forEach(child => {
            acc.add(child);
            _collectDescendants(child, acc);
        });
        return acc
    }

    function countLeafNodes(node: TreeNode): number {
        return node.children.reduce((count, child) => {
            if (child.children.length === 0) {
                return count + 1;
            } else {
                return count + countLeafNodes(child);
            }
        }, 0);
    }

    function collectAncestors(node: TreeNode): Set<TreeNode> {
        const ancSet = new Set<TreeNode>()
        let currentNode: TreeNode | null = node.parent
        while (currentNode !== null) {
            ancSet.add(currentNode)
            currentNode = currentNode.parent
        }

        return ancSet
    }

    function nodeIsLeaf(node: TreeNode) {
        return node.children.length === 0
    }

    function setNewParent(node: TreeNode, parent: TreeNode) {
        if (node.parent) {
            node.parent.children = node.parent?.children.filter((child) => !(child === node))
        }
        addChildSorted(parent, node)
        node.parent = parent
    }

    function addChildSorted(parent: TreeNode, newChild: TreeNode) {
        newChild.parent = parent;
        const index = findInsertIndex(parent.children, newChild, compareNodeSort);
        parent.children.splice(index, 0, newChild);
    }

    function compareNodeSort(a: TreeNode, b: TreeNode): number {
        return a.name.localeCompare(b.name);
    }

    function findInsertIndex(
        children: TreeNode[],
        newChild: TreeNode,
        compare: (a: TreeNode, b: TreeNode) => number
    ): number {
        let left = 0;
        let right = children.length - 1;

        while (left <= right) {
            const middle = Math.floor((left + right) / 2);
            const comparison = compare(children[middle], newChild);

            if (comparison < 0) {
                left = middle + 1;
            } else {
                right = middle - 1;
            }
        }

        return left;
    }

    function getNodeLevel(node: TreeNode) {
        return collectAncestors(node).size
    }

    return {collectAncestors, collectDescendants, countLeafNodes, nodeIsLeaf, setNewParent, getNodeLevel}
}



