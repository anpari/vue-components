import {computed, Ref} from "vue";
import {TreeNode} from "@/tree-view/types.ts";
import {useNodeTreeCollapse} from "@/tree-view/nodeTree.collapse.ts";
import {useNodeTreeSelection} from "@/tree-view/nodeTree.select.ts";
import {useNodeTreeDragDrop} from "@/tree-view/nodeTree.dragdrop.ts";

export function useNodeTree(nodeTree: Ref<TreeNode[]>) {

    const {
        collapsedNodes,
        toggleNodeCollapse,
        expandAll,
        collapseAll
    } = useNodeTreeCollapse(nodeTree)

    const {
        selectedLeafNodes,
        selectedLeafCount,
        toggleNodeSelection,
        selfOrLeafSelected,
        getLeafCount,
        setNodesSelected
    } = useNodeTreeSelection()

    const dragdrop = useNodeTreeDragDrop()

    const flattenedNodes = computed(() => {
        const flattenTree = (nodes: TreeNode[], level = 0): TreeNode[] => {
            return nodes.reduce((acc: TreeNode[], node: TreeNode) => {
                const isCollapsed = collapsedNodes.value.has(node.path);
                acc.push(node);
                if (!isCollapsed && node.children) {
                    acc.push(...flattenTree(node.children, level + 1));
                }
                return acc;
            }, []);
        };

        return flattenTree(nodeTree.value);
    });

    return {
        flattenedNodes,
        collapsedNodes,
        toggleNodeCollapse,
        expandAll,
        collapseAll,
        selectedLeafNodes,
        selectedLeafCount,
        toggleNodeSelection,
        selfOrLeafSelected,
        getLeafCount,
        setNodesSelected,
        dragdrop
    }
}

