import {computed, Ref} from "vue";
import {TreeNode} from "@/tree-view/types.ts";
import {useTreeNode} from "@/tree-view/treeNode.ts";

const {collectAncestors} = useTreeNode()

export function useNodeTreeFilter() {

    function filterNodesByText(filterText: Ref<string>, nodes: Ref<TreeNode[]>,) {
        return computed(
            () => {
                const filteredPaths = new Set<string>()
                nodes.value.forEach((node) => {
                    if (node.name.toLowerCase().includes(filterText.value.toLowerCase())) {
                        collectAncestors(node).add(node).forEach(
                            (anc) => filteredPaths.add(anc.path)
                        )
                    }
                })

                return nodes.value.filter(
                    (node) => filteredPaths.has(node.path)
                )
            }
        )
    }

    function filterSelectedNodes(selfOfLeafSelected: (node: TreeNode) => boolean, nodes: Ref<TreeNode[]>) {
        return computed(() => {
            return nodes.value.filter((node) => selfOfLeafSelected(node))
        })
    }

    return {filterNodesByText, filterSelectedNodes}
}