import {Ref, ref} from "vue";
import {CountByPath, LeafNodePaths, TreeNode} from "@/tree-view/types.ts";
import {useTreeNode} from "@/tree-view/treeNode.ts";

const {collectAncestors, collectDescendants, countLeafNodes, nodeIsLeaf} = useTreeNode()


export function useNodeTreeSelection() {

    const selectedLeafNodes: Ref<LeafNodePaths> = ref(new Set<string>());  // selected leaf node paths
    const selectedLeafCount = ref<CountByPath>({})  // counts selected leaf nodes by path
    const leafCountCache = ref<CountByPath>({})  // caches total leaf count by node path

    function toggleNodeSelection(node: TreeNode) {
        if (!selfOrLeafSelected(node)) return selectNode(node)
        if (!selfAndLeafsSelected(node)) return selectNode(node)
        return unselectNode(node)
    }

    function unselectNode(node: TreeNode) {
        const leafDecrement = nodeIsLeaf(node) ? 1 : getLeafCount(node)
        collectAncestors(node).forEach((anc) => decrementLeafCount(anc, leafDecrement))
        collectDescendants(node).add(node).forEach((desc) => {
            if (nodeIsLeaf(desc)) {
                selectedLeafNodes.value.delete(desc.path)
            } else {
                delete selectedLeafCount.value[desc.path]
            }
        })
    }

    function selectNode(node: TreeNode) {
        if (nodeIsLeaf(node)) {
            _setNodeSelected(node)
        } else {
            collectDescendants(node, true).forEach((leaf) => {
                _setNodeSelected(leaf)
            })
        }
    }

    function _setNodeSelected(node: TreeNode) {
        if (!nodeIsLeaf(node)) return
        if (nodeIsSelected(node)) return
        selectedLeafNodes.value.add(node.path)
        collectAncestors(node).forEach((anc) => incrementLeafCount(anc))
    }

    function incrementLeafCount(node: TreeNode, by: number = 1) {
        if (!(node.path in selectedLeafCount.value)) selectedLeafCount.value[node.path] = 0
        selectedLeafCount.value[node.path] += by
    }

    function decrementLeafCount(node: TreeNode, by: number = 1) {
        selectedLeafCount.value[node.path] -= by
        if (selectedLeafCount.value[node.path] === 0) delete selectedLeafCount.value[node.path]
    }

    function getLeafCount(node: TreeNode) {
        if (!(node.path in leafCountCache)) leafCountCache.value[node.path] = countLeafNodes(node)
        return leafCountCache.value[node.path]
    }

    function selfOrLeafSelected(node: TreeNode) {
        // checks if the node itself or any leaf nodes are selected
        if (selectedLeafNodes.value.has(node.path)) return true
        return node.path in selectedLeafCount.value
    }

    function selfAndLeafsSelected(node: TreeNode) {
        // checks if the node itself and all leaf nodes are selected, if applicable
        if (nodeIsLeaf(node)) return nodeIsSelected(node)
        return selectedLeafCount.value[node.path] === getLeafCount(node)
    }

    function nodeIsSelected(node: TreeNode) {
        return selectedLeafNodes.value.has(node.path)
    }

    function setNodesSelected(nodeTree: TreeNode[], paths: LeafNodePaths) {
        clearSelection()
        nodeTree.forEach((node: TreeNode) => {
            collectDescendants(node, true).forEach(
                (leaf) => {
                    if (paths.has(leaf.path)) selectNode(leaf)
                }
            )
        })
    }

    function clearSelection() {
        selectedLeafNodes.value.clear()
        selectedLeafCount.value = {}
        leafCountCache.value = {}
    }

    return {
        selectedLeafNodes,
        selectedLeafCount,
        toggleNodeSelection,
        selfOrLeafSelected,
        getLeafCount,
        setNodesSelected
    }
}