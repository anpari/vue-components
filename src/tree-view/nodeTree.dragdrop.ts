import {Ref, ref} from "vue";
import {TreeNode} from "@/tree-view/types.ts";
import {useTreeNode} from "@/tree-view/treeNode.ts";

const {setNewParent, collectDescendants} = useTreeNode()

type MaybeNode = TreeNode | null

export type NodeTreeDragdrop = {
    draggedNode: Ref<MaybeNode>,
    dragOverNode: Ref<MaybeNode>,
    handleDragStart: (e: Event, node: TreeNode) => void,
    handleDragEnter: (node: TreeNode) => void,
    handleDragLeave: (node: TreeNode) => void,
    handleDragOver: (e: Event) => void,
    handleDrop: (e: Event, node: TreeNode) => void,
    handleDragEnd: () => void
}

export function useNodeTreeDragDrop(): NodeTreeDragdrop {

    const draggedNode = ref<MaybeNode>(null)
    const draggedElement = ref<HTMLElement | null>(null)
    const dragOverNode = ref<MaybeNode>(null)

    function handleDragStart(e: Event, node: TreeNode) {
        draggedNode.value = node
        draggedElement.value = e.target as HTMLElement
    }

    function handleDragEnter(node: TreeNode) {
        dragOverNode.value = node
    }

    function handleDragLeave(node: TreeNode) {
        // great thread about handling enter/leave issues when child nodes are involved
        // https://stackoverflow.com/questions/7110353/html5-dragleave-fired-when-hovering-a-child-element
        // note: implemented the simple CSS solution since it works currently without ugly hacks:
        // https://stackoverflow.com/a/18582960
        if (node === dragOverNode.value) dragOverNode.value = null
    }

    function handleDragOver(e: Event) {
        e.preventDefault();
    }

    function handleDrop(e: Event, newParent: TreeNode) {
        e.stopPropagation(); // stops the browser from redirecting.
        if (!draggedNode.value) return;
        if (
            draggedNode.value.parent === newParent // new parent is node
            || collectDescendants(draggedNode.value, false).has(newParent)  // new parent is descendant
        ) return
        setNewParent(draggedNode.value, newParent)
    }

    function handleDragEnd() {
        draggedNode.value = null
        dragOverNode.value = null
        if (draggedElement.value !== null) {
            draggedElement.value.draggable = false
        }
    }

    return {
        draggedNode,
        dragOverNode,
        handleDragStart,
        handleDragEnter,
        handleDragLeave,
        handleDragOver,
        handleDrop,
        handleDragEnd,
    }
}
