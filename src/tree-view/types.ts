export type TreeNode = {
    path: string
    name: string
    separator: string
    parent: TreeNode | null
    children: TreeNode[]
}
export type LeafNodePaths = Set<string>
export type CountByPath = { [key: string]: number }