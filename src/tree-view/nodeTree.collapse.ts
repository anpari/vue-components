import {Ref, ref} from "vue";
import {CollapsedNodes, TreeNode} from "@/tree-view/types.ts";


export function useNodeTreeCollapse(nodeTree: Ref<TreeNode[]>) {

    const collapsedNodes: Ref<CollapsedNodes> = ref(new Set<string>());

    function toggleNodeCollapse(node: TreeNode) {
        if (collapsedNodes.value.has(node.path)) {
            collapsedNodes.value.delete(node.path);
        } else {
            collapsedNodes.value.add(node.path);
        }
    }

    function expandAll() {
        collapsedNodes.value.clear()
    }

    function collapseAll(){
        collapseNodesWithChildren(nodeTree.value, collapsedNodes)
    }

    return {collapsedNodes, toggleNodeCollapse, expandAll, collapseAll}
}

function collapseNodesWithChildren(nodes: TreeNode[], collapsedNodes: Ref<Set<string>>): void {
    nodes.forEach(node => {
        if (node.children.length > 0) {
            collapsedNodes.value.add(node.path);
            collapseNodesWithChildren(node.children, collapsedNodes);
        }
    });
}

