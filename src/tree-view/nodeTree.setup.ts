import {TreeNode} from "@/tree-view/types.ts";

export function buildTree(paths: string[], separator: string = '/'): TreeNode[] {
    const rootNode: { [key: string]: TreeNode } = {};

    paths.forEach((path) => {
        const parts = path.split(separator);
        let currentLevel = rootNode;

        parts.forEach((part, index) => {
            const isLeaf = index === parts.length - 1;
            const nodePath = parts.slice(0, index + 1).join(separator);

            if (!currentLevel[part]) {
                const parent: TreeNode | null = (index > 0) ? currentLevel['..'] : null;
                const node: TreeNode = {
                    path: nodePath,
                    name: part,
                    separator: separator,
                    parent: parent,
                    children: []
                };
                if (parent !== null) parent.children.push(node);
                currentLevel[part] = node;

            }

            if (!isLeaf) {
                if (!currentLevel[part].children) currentLevel[part].children = [];

                currentLevel = currentLevel[part].children
                    .reduce((acc: { [key: string]: TreeNode }, child: TreeNode) => {
                            acc[child.name] = child;
                            return acc;
                        },
                        {'..': currentLevel[part]} // Keep a reference to parent for easier linking
                    );
            }
        });
    });

    // Extracting root nodes from the hash table
    return Object.values(rootNode);
}