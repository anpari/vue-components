
// https://stackoverflow.com/a/74226286
// should use 'a.size === b.size && a.isSubsetOf(b)' as soon as standard is released
export function isEqualSets(a: Set<any>, b: Set<any>) {
    if (a === b) return true;
    if (a.size !== b.size) return false;
    for (const value of a) if (!b.has(value)) return false;
    return true;
}